---
name: Diagrams
---

Diagrams are used to illustrate the functionality of the GitLab product. They act like a manual, representing complex ideas step-by-step. They should be structured and visually clear, while using as minimal detail as possible.

### Color

- Diagrams use plenty of negative space to make the story easy to follow and understand.
- Colors should be used to provide additional instruction and clarity; consider how some colors can carry a positive or negative connotation, how they can be used to group related elements, or how they can aid in the direction and flow of the diagram.

### Linework

- Diagrams are mostly constructed of linework and should match our guidelines regarding lines and stroke weight.
- Motion lines can be used as accents; they should help tell the story and/or guide the user’s eye through the diagram’s flow.
- Line breaks give the feel of openness throughout the diagram. Lines should break before touching an icon, unless the icon is contained within a circle.
- Dashed lines can be used to connect type and visual elements.

### Icons

- Marketing icons should be used and contained within a circle with 25% padding around the icon.

### Logo

- A single-color tanuki (refer to [logomark variations](/brand-logo/logomark#variations)) can be used sparingly in diagrams to denote a product feature.
- When representing how the GitLab product works in conjunction with or compared to another product, use the core logo.

<figure-img alt="A light and dark background version of a diagram" label="Diagram sample" src="/img/brand/diagram-example.svg"></figure-img>

## Infographics

Infographics are data-driven pieces that merge illustrations within the messaging. They typically have a theme and the graphics bring this to life to tell the story of the content. They have a distinct flow that formats the content into digestible pieces and brings the reader to specific conclusions.

Below are the various elements that comprise an infographic and our approach to each:

- **Format:** Set up the file to support a long scrolling experience throughout since infographics typically have a vertical flow.
- **Header/footer:** Add a distinct header and footer to clearly mark the collateral as GitLab. The header should clearly explain what the infographic is about and the footer should include a call-to-action.
- **Theme:** Infuse the theme throughout the design and illustrations of the infographic to strengthen the storytelling.
- **Content:** Break down the content into digestible pieces and organize accordingly. Readers should feel like each piece of content naturally flows into the next.
- **Flow:** Be intentional when structuring the content and visuals so that readers are smoothly guided from top to bottom. Lines, shapes, and background color can also be used to section content throughout.
- **Data:** Differentiate stats and copy to add hierarchy and create visual interest. Data points can be enlarged and set in GitLab Mono font.
- **Graphics:** Show, don’t tell, when it comes to infographics. Find ways to visualize the content through icons and illustrations, instead of defaulting to a content-heavy format.
- **Color:** Create focus and emphasis within the piece by applying colors intentionally. Overall, colors should be limited to help tone down the busyness of the piece and keep readers focused on the overall story.
